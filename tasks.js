'use strict';
/*
    В этом задании нельзя менять разметку, только писать код в этом файле.
 */

/**
*   1. Удали со страницы элемент с id "deleteMe"
**/

function removeBlock() {
  document.querySelector('#deleteMe').remove();
}

/**
 *  2. Сделай так, чтобы во всех элементах с классом wrapper остался только один параграф,
 *  в котором будет сумма чисел из всех параграфов.
 *
 *  Например, такой элемент:
 *
 *  <div class="wrapper"><p>5</p><p>15</p><p>25</p><p>35</p></div>
 *
 *  должен стать таким
 *
 *  <div class="wrapper"><p>80</p></div>
 */

function calcParagraphs() {
  const wraps = document.querySelectorAll('.wrapper');
  for(let wrap of wraps) {
    const wrapChildren = wrap.querySelectorAll('p');
    let sum = 0;
    for(let child of wrapChildren) {
      sum += Number(child.innerHTML);
      child.remove();
    }
    const newP = document.createElement('p');
    newP.innerHTML = sum;
    wrap.appendChild(newP);
  }
}

/**
 *  3. Измени value и type у input c id "changeMe"
 *
 */

function changeInput() {
  const input = document.getElementById("changeMe");
  input.type = "text";
  input.value = "Успешно изменено";
}

/**
 *  4. Используя функции appendChild и insertBefore дополни список с id "changeChild"
 *  чтобы получилась последовательность <li>0</li><li>1</li><li>2</li><li>3</li>
 *
 */

function appendList() {
  const li1 = document.createElement('li');
  const li3 = document.createElement('li');
  li1.innerHTML = 1;
  li3.innerHTML = 3;
  const list = document.getElementById('changeChild');
  list.appendChild(li3);
  list.insertBefore(li1, list.children[1]);

}

/**
 *  5. Поменяй цвет всем div с class "item".
 *  Если у блока class "red", то замени его на "blue" и наоборот для блоков с class "blue"
 */

function changeColors() {
  const items = document.querySelectorAll('.item');
  for(let item of items) {
    item.classList.toggle("red");
    item.classList.toggle("blue");
  }
}

removeBlock();
calcParagraphs();
changeInput();
appendList();
changeColors();
